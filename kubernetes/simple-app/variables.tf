variable "image" {
  type = string
}

variable "name" {
  type = string
}

variable "env" {
  type = map(string)
  default = {}
}

variable "files" {
  type = list(object({path = string, content = string}))
  default = []
}

variable "maxMemory" {
  type = string
  default = "50Mi"
}

variable "pullSecretsName" {
  type = string
}

variable "port" {
  type = number
  default = null
}

variable "namespace" {
  type = string
}