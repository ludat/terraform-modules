locals {
  filesWithKey = [
  for file in var.files: {
    path = file.path
    content = file.content
    key = trim(replace(file.path, "/[^a-z]+/", "-"), "-")
  }
  ]
}

resource "kubernetes_service" "service" {
  depends_on = [kubernetes_deployment.deploy]
  count = var.port != null ? 1 : 0
  metadata {
    name = var.name
    namespace = var.namespace
  }
  spec {
    selector = {
      app = var.name
    }
    port {
      target_port = var.port
      port = 80
    }
  }
}

resource "kubernetes_config_map" "configmap" {
  metadata {
    generate_name = "${var.name}-config"
    namespace = var.namespace
  }

  data = {
  for file in local.filesWithKey:
  file.key => file.content
  }
}

resource "kubernetes_deployment" "deploy" {
  metadata {
    name = var.name
    namespace = var.namespace
    labels = {
      app = var.name
    }
  }
  spec {
    selector {
      match_labels = {
        app = var.name
      }
    }
    replicas = 1
    template {
      metadata {
        labels = {
          app = var.name
        }
      }
      spec {
        volume {
          name = "config"
          config_map {
            name = kubernetes_config_map.configmap.metadata[0].name

          }
        }
        image_pull_secrets {
          name = var.pullSecretsName
        }
        container {
          resources {
            limits = {
              // cpu = ""
              memory = var.maxMemory
            }
            // requests {
            //   cpu = ""
            //   memory = var.maxMemory
            // }
          }
          name = "main"
          image = var.image

          dynamic "volume_mount" {
            for_each = local.filesWithKey

            content {
              mount_path = volume_mount.value.path
              name = "config"
              sub_path = volume_mount.value.key
            }
          }

          dynamic "env" {
            for_each = var.env

            content {
              name = env.key
              value = env.value
            }
          }
        }
      }
    }
  }
}
