variable "username" {
  type = string
}

variable "password" {
  type = string
}

variable "namespace" {
  type = string
}

variable "name" {
  type = string
  default = "gitlab-pull-secret"
}

