resource "kubernetes_secret" "image-puller" {
  metadata {
    name = var.name
    namespace = var.namespace
  }

  type = "kubernetes.io/dockerconfigjson"

  data = {
    ".dockerconfigjson" = jsonencode({
      "auths" : {
        "registry.gitlab.com" : {
          username = var.username
          password = var.password
          auth = base64encode(join(":", [
            var.username,
            var.password]))
        }
      }
    })
  }
}
